#!/bin/bash

# --> jobs name
#PBS -N rockstar
 
# --> we request 8 nodes with 12 cores per nodes = 8 x 12 = 96 cores in total
#PBS -l nodes=1:ppn=4

# --> The maximum wall-clock time during which this job can run (hh:mm:ss)
#PBS -l walltime=00:09:00

# -- > output log file name

# --> output error file name
#PBS -o "log_unigrid.txt"

#PBS -e "err-unigrid.txt"   


export PSM_SHAREDCONTEXTS_MAX=8
# change to submission jobs directory
cd $PBS_O_WORKDIR

# command to launch the run
mpiexec -f ${PBS_NODEFILE} -np ${PBS_NP} python test.py >log.txt
