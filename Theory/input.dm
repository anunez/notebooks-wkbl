### INPUT file
### data shuld separated by ONE (1) blank space
###
[general]
## velocities
# km/s
v_sun     =   220.  
v_esc     =   544.
v_esc     =   394.
v_esc     =   294.
v_0shm    =   270.
v_0lin    =   267.
# *v_esc km/s
v_0mao    =   0.13
## local density
# GeV/m^3
rho       =   0.3
## Cross sections
# /0.3894 GeV WIMP scattering cross section SD from ellis
sigma_sd  =   2.19e-6 
# /0.3894 GeV WIMP scattering cross section SI from ellis
sigma_si  =   2.85e-9
## Proton mass
# GeV
m_p       =   938.272e-3
## #r_h ((0.9*(m_p**(1/3)))+ 0.3)*(10**(-13)) cm nuclear radius
