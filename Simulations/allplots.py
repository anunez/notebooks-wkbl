from scipy.stats import rv_continuous
from scipy.interpolate import interp1d
from matplotlib.patches import Circle
from scipy.special import gamma
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable
from numpy import exp, sqrt
from scipy.integrate import quad, dblquad
import matplotlib.patches as patches
from itertools import product
from scipy.integrate import quad
import scipy.optimize as optimize
import matplotlib.pyplot as plt
import matplotlib as mpl
import sys
from py_unsio import *
import os
import wkbl
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
import wkbl.astro.nbody_essentials as nbe
import cfalcon
CF =cfalcon.CFalcon()
#import iminuit
#from iminuit import Minuit, describe, Struct
from matplotlib.colors import LogNorm

sim_path = sys.argv[1]
place = sys.argv[2]

def load_sim(path):
    print "############################################### "
    myhalo = wkbl.Galaxy_Hound(path)
    cen = nbe.real_center(myhalo.st.pos3d,myhalo.st.mass)    
    #cen = myhalo.dm.Clumps.pos3d[myhalo.dm.Clumps.cell==myhalo.dm.Clumps.cell.max()]
    myhalo.center_shift(cen)
    myhalo.r_virial(600)
    print "r200 = {0:.2f}".format(myhalo.r200)
    print "stellar mass = {0:.3e}".format( myhalo.st.total_m)
    print "dark matter mass = {0:.3e}".format(myhalo.dm.total_m)
    print "Z = {0:.2f}".format(myhalo.p.Z)
    nucenter = nbe.real_center(myhalo.dm.pos3d, myhalo.dm.mass)
    myhalo.center_shift(nucenter)
    myhalo.redefine(4.5)
    ok,myhalo.dm.rho,_= CF.getDensity(np.array(myhalo.dm.pos3d.reshape(len(myhalo.dm.pos3d)*3),\
                                    dtype=np.float32), myhalo.dm.mass)
    print "dm density"
    #ok,myhalo.st.rho,_= CF.getDensity(np.array(myhalo.st.pos3d.reshape(len(myhalo.st.pos3d)*3),\
    #                                dtype=np.float32), myhalo.st.mass)
    print "stars density"
    return myhalo

def face_on_dm(sim,lims,points):
    edges = np.linspace(lims[0],lims[1],points)
    H, xedges, yedges = np.histogram2d(sim.dm.pos3d[:,0], 
                                       sim.dm.pos3d[:,1],
                                       bins=(edges, edges),
                                       weights=sim.dm.mass)
    result = H.T
    return result, edges

def face_on_st(sim,lims,points,thikness=.5):
    disk = (np.abs(sim.st.pos3d[:,2])<thikness)
    edges = np.linspace(lims[0],lims[1],points)
    H, xedges, yedges = np.histogram2d(sim.st.pos3d[disk,0], 
                                       sim.st.pos3d[disk,1],
                                       bins=(edges, edges),
                                       weights=sim.st.mass[disk])
    result = H.T
    return result, edges

def face_on_gs(sim,lims,points,thikness=.9):
    disk = (np.abs(sim.gs.pos3d[:,2])<thikness)
    edges = np.linspace(lims[0],lims[1],points)
    H, xedges, yedges = np.histogram2d(sim.gs.pos3d[disk,0], 
                                       sim.gs.pos3d[disk,1],
                                       bins=(edges, edges),
                                       weights=sim.gs.mass[disk])
    result = H.T
    return result, edges

def edge_on_st(sim,lims,points):
    #disk = sim.st.pos3d[:,2]
    edges = np.linspace(lims[0],lims[1],points)
    H, xedges, yedges = np.histogram2d(sim.st.pos3d[:,0], 
                                       sim.st.pos3d[:,2],
                                       bins=(edges, edges),
                                       weights=sim.st.mass)
    result = H.T
    return result, edges
    
def edge_on_gs(sim,lims,points):
    edges = np.linspace(lims[0],lims[1],points)
    H, xedges, yedges = np.histogram2d(sim.gs.pos3d[:,0], 
                                       sim.gs.pos3d[:,2],
                                       bins=(edges, edges),
                                       weights=sim.gs.mass)
    result = H.T
    return result, edges
    



# mosted et al.
def M_1(z):
    M10 ,M11 = 11.590, 1.195
    log = M10 + M11*(z / (z+1))
    return 10**(log)

def N(z):
    N10 ,N11 = 0.0351, -0.0247
    return N10 + N11*(z / (z+1))


def beta(z):
    B10 ,B11 = 1.376, -0.826
    return B10 + B11*(z / (z+1))


def gamma(z):
    G10 ,G11 = 0.608, 0.329
    return G10 + G11*(z / (z+1))

def mm(M,z=0):
    one = ( M / M_1(z))**(-beta(z))
    two = ( M / M_1(z))**gamma(z)
    return 2*N(z) * M / (one +two)

def alpha(m):
    return 0.15 / np.log10(m)

M = np.logspace(10,14,50)
m = mm(M)
al = np.sqrt(m)#alpha(m)

def profile_dm_baryons(myhalo):
    fig, ax= plt.subplots(figsize=[7,6])
    ax.set_xlim([1e-2,10**2.5])
    ax.set_ylim([1,1e12])
    #ax.scatter(myhalo.st.r, myhalo.st.rho,s=0.5,alpha=0.5,lw=0,c='y')
    ax.scatter(1e20,1e20,c='y',alpha=0.5, label="stars")
    ax.scatter(1e20,1e20,c='b',alpha=0.5, label="DM")
    
    ax.scatter(myhalo.dm.r, myhalo.dm.rho,s=0.5,alpha=0.5,lw=0)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_ylim([1e0,1e12])
    ax.set_xlabel(r"r [kpc]",fontsize=20)
    ax.set_ylabel(r"$\rho$ [M$_{\odot}$ kpc$^{-3}$]",fontsize=20)
    ax.set_title("",fontsize=15)
    ####
    legend = ax.legend(loc='bottom left', ncol=1, shadow=False, fontsize=12)
    plt.savefig("profiles.png")
    return 0



def morphology(myhalo):
    SF130_faceon, edges = face_on_dm(myhalo,[-1.2*myhalo.r200,1.2*myhalo.r200],300)
    length = 15.
    fig,[[ax,ax1,ax2],[ax3,ax4,ax5]] = plt.subplots(2,3,figsize=[22,13])
    fig.tight_layout(w_pad=3)

    #######################################################################################################################3
    mass_1 = ax.imshow(SF130_faceon+1e3, interpolation='nearest', origin='low',cmap="magma",
                           extent=[edges[0], edges[-1], edges[0], edges[-1]],
                       norm=LogNorm(vmin=1e7)
                      )
    divider = make_axes_locatable(ax)

    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(mass_1,cax=cax,label=r'mass [M$_{\odot}$]')

    ax.add_artist(Circle(xy=(0, 0),radius=myhalo.r200,color='w',ls='--',lw=1.7,fill=False))
    ax.text(-100,1.1*myhalo.r200,r"R$_{200}$ = "+str(int(myhalo.r200))+" Kpc ",color='w',fontsize=15)
    #######################################################################################################################3

    SF1140_faceOn,edges = face_on_st(myhalo,[-length,length],200)#H.T

    mass_2 = ax1.imshow(SF1140_faceOn+1e3, interpolation='nearest', origin='low',cmap="bone",
                           extent=[edges[0], edges[-1], edges[0], edges[-1]],
                       norm=LogNorm(vmin=1e5)
                      )

    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(mass_2,cax=cax,label=r'mass [M$_{\odot}$]')
    #######################################################################################################################3

    SF1140_edgeOn,edges = edge_on_st(myhalo,[-length,length],150)#H.T


    mass_2 = ax2.imshow(SF1140_edgeOn+1e3, interpolation='nearest', origin='low',cmap="bone",
                           extent=[edges[0], edges[-1], edges[0], edges[-1]],
                       norm=LogNorm(vmin=1e5)
                      )

    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(mass_2,cax=cax,label=r'mass [M$_{\odot}$]')

    SF1140_faceOn,edges= face_on_gs(myhalo,[-length,length],150)#H.T

    mass_2 = ax4.imshow(SF1140_faceOn+1e5, interpolation='nearest', origin='low',
                           extent=[edges[0], edges[-1], edges[0], edges[-1]],
                       norm=LogNorm(vmin=1e5)
                      )

    divider = make_axes_locatable(ax4)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(mass_2,cax=cax,label=r'mass [M$_{\odot}$]')

    #######################################################################################################################3

    SF1140_edgeOn,edges = edge_on_gs(myhalo,[-length,length],150)#H.T


    mass_2 = ax5.imshow(SF1140_edgeOn, interpolation='nearest', origin='low',
                           extent=[edges[0], edges[-1], edges[0], edges[-1]],
                       norm=LogNorm(vmin=1e5)
                      )

    divider = make_axes_locatable(ax5)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(mass_2,cax=cax,label=r'mass [M$_{\odot}$]')


    ax3.scatter(myhalo.dm.r, myhalo.dm.rho,s=0.5,alpha=0.5,lw=0)
    ax3.set_xscale("log")
    ax3.set_yscale("log")
    ax3.set_ylim([1e0,1e12])
    ax3.set_xlabel(r"r [kpc]",fontsize=20)
    ax3.set_ylabel(r"$\rho$ [M$_{\odot}$ kpc$^{-3}$]",fontsize=20)
    ax3.set_title("sf_model = 0",fontsize=15)
    plt.savefig(place+"/morphology.png")
    return 0


def main():
    myhalo = load_sim(sim_path)
    #profile_dm_baryons(myhalo)
    morphology(myhalo)

if __name__ == "__main__":
        main()

