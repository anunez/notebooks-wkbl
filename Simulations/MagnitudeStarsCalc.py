
import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import numpy as np
from py_unsio import *
from operator import itemgetter, attrgetter
from math import *
from StringIO import StringIO

##### Cette routine calculera la luminosite de chaque single stellar population integree par unite de masse
####en se basant sur le fichier a fournir (sous le format de CMD http://stev.oapd.inaf.it/cgi-bin/cmd). Elle
####creera un #fichier nemo avec la luminosite dans la bande couleur choisie, par etoile en fonction de son
####age et de son metallicite, et la mettra dans le champ densite. #####

myoutputs=[1274]
myrepert="/data/Pol-code/SIMUS/Box20Mpc-Zoom-MWGalax-MUSIC/256-DM+Baryons-Zoom4-Halo9-BigcenterBox-100%rad-32noeuds-run13/"
comp="all"
band="r"
StopFlag=1 # if stopflag=1, doesn t generate output files
MetalFlag=1 #if set to one, takes into account the metallicity of stars. If set to 0, indicate one met for all your stars below.

def MagCalc(myage,myMagAge,myMag,SolMagBand,mymass,mymetal,myMagMet):
    #metind=myMagMet[np.where(abs(mymetal-myMagMet)<=abs(mymetal-myMagMet).min())]
    #print 'mymetal',mymetal
    #print 'npwhere',np.where(abs(mymetal-myMagMet)<=abs(mymetal-myMagMet).min())
    #print 'metind',metind.shape
    #print 'myMagMetSelected',myMagMet[np.where(abs(mymetal-myMagMet)<=abs(mymetal-myMagMet).min())]

    #Select first 2 matrices by metal...
    myMagAgeSelected=myMagAge[np.where(abs(mymetal-myMagMet)<=abs(mymetal-myMagMet).min())]
    myMagSelected=myMag[np.where(abs(mymetal-myMagMet)<=abs(mymetal-myMagMet).min())]
    #print 'myMagAgeSelected ',myMagAgeSelected,myMagAgeSelected.shape
    #print 'myMagSelected ',myMagSelected,myMagSelected.shape

    #...then search for the closest star age in the selected list
    ind=np.argmin(abs(myage-myMagAgeSelected))
    #print 'star age:',myage,'Magnit',myMagAgeSelected[ind]
    #print 'log10(mymass)',log10(mymass)
    #Calculate Magnitude
    mag=myMagSelected[ind]-2.5*log10(mymass)
    #print 'myMagSelected[ind]',myMagSelected[ind],'calc mag:',mag
    #Calculate luminosity
    lum=10**(-(mag-SolMagBand)/2.5)  #in Solar Luminosities
    return mag,lum

print(len(myoutputs))
for i in range(len(myoutputs)):
    a=myoutputs[i]
    count='0000'+str(a)
    if a >= 10:
        count='000'+str(a)
    if a >= 100:
        count='00'+str(a)
    if a >= 1000:
        count='0'+str(a)
    print("output =",'output_'+count)
    vars=dict()
    with open('output_'+count+'/info_'+count+'.txt') as outinfo:
        for line in outinfo:
            print(line)
            eq_index = line.find('=')
            if eq_index == -1:
                continue
            var_name = line[:eq_index].strip()
            number = float(line[eq_index + 1:].strip())
            vars[var_name] = number
            if var_name == "unit_t":
                break
    print(vars)
    aexp=vars["aexp"]
    print('aexp= ',aexp)
    #simutokpc=vars["unit_l"]/(3.08*10**18)/1000
    #simutoMsun=(vars["unit_d"]*(vars["unit_l"])**3)/1000/(1.99844*10**30)
    Msunkg = 1.99844*10**30
    pctocm = 3.08567758*10**18
    cmtopc = 1/pctocm
    unitl=vars["unit_l"]/(3.08*10**18)*pctocm
    unitd=vars["unit_d"]/(pctocm/(3.08*10**18))**3
    simutokpc=unitl/pctocm/1000
    simutoMsun=(unitd*unitl**3)/1000/Msunkg
    unitsimutoMsunkpc3=unitd*pctocm**3/(1000*Msunkg)


    print("simutokpc= ",simutokpc)
    print("simutoMsun= ",simutoMsun)
        
    file=myrepert+'myhalo-out1274-st-rot.met.Myrage.nemo.dens'
    nameout=myrepert+"myhalo-out1274-st-rot.met.Myrage.nemo.Mag-"+band+"-band"
    uns=CunsIn(file,comp,"all");
    # load frame
    ok=uns.nextFrame("")
    print ok
    # get data
    ok,data=uns.getArrayF(comp,"pos")
    print ok
    print data
    # reshap 1D to 2D
    data=np.reshape(data,(-1,3))

    print data
    # get col 1 to x
    posx = data[:,0]
    # get col 2 to y
    posy = data[:,1]
    # get col 3 to z
    posz = data[:,2]

    r=np.sqrt(posx**2+posy**2+posz**2)
    #load velocity
    ok,data=uns.getArrayF(comp,"vel")
    print ok
    data=np.reshape(data,(-1,3))
    # get col 1 to x
    vx = data[:,0]
    # get col 2 to y
    vy = data[:,1]
    vz = data[:,2]

    ok,dens=uns.getArrayF(comp,"rho")
    #print dens.max()
    #print dens.min()

    ok,mass=uns.getArrayF(comp,"mass")
    mass=mass*simutoMsun
    ok,age=uns.getArrayF(comp,"pot")

    ok,data=uns.getArrayF(comp,"acc")
    data=np.reshape(data,(-1,3))
    metal = data[:,1]
    #ok,metal=uns.getArrayF(comp,"acc")
    print 'metal :',metal,'metal min max',metal.min(),metal.max()
    
    #if MetalFlag==0:
    #    #Indicate the assumed metallicity of the stars
    #    metaltmp=0.0080*np.ones(len(mass))


    #Go and read in the photometric file
    MagMet=MagAge=MagU=MagB=MagV=MagR=MagI=MagJ=MagH=MagK=np.array([0])
    if MetalFlag==0:
        with open('/data/Pol-code/LuminosityCalc/GoodFiles/met0.02-age1.76-10.13.dat') as maginfo:
            for line in maginfo:
                 #print(line)
                 
                 result=np.genfromtxt(StringIO(line),comments='#',delimiter=None)
                 #magage=np.append(magage,result[0])
                 #print result
                 if len(result)>0:
                     MagMet=np.append(MagMet,result[0])
                     MagAge=np.append(MagAge,result[1])
                     MagU=np.append(MagU,result[3])
                     MagB=np.append(MagB,result[4])
                     MagV=np.append(MagV,result[5])
                     MagR=np.append(MagR,result[6])
                     MagI=np.append(MagI,result[7])
                     MagJ=np.append(MagJ,result[8])
                     MagH=np.append(MagH,result[9])
                     MagK=np.append(MagK,result[10])
        MagMet=MagMet[1:]
        MagAge=MagAge[1:]/10**6
        MagU=MagU[1:]
        MagB=MagB[1:]
        MagV=MagV[1:]
        MagR=MagR[1:]
        MagI=MagI[1:]
        MagJ=MagJ[1:]
        MagH=MagH[1:]
        MagK=MagK[1:]
    else:
        with open('/data/Pol-code/LuminosityCalc/GoodFiles/AllData.dat') as maginfo:
            for line in maginfo:
                 #print(line)
                 
                 result=np.genfromtxt(StringIO(line),comments='#',delimiter=None)
                 #magage=np.append(magage,result[0])
                 #print result
                 if len(result)>0:
                     MagMet=np.append(MagMet,result[0])
                     MagAge=np.append(MagAge,result[1])
                     MagU=np.append(MagU,result[3])
                     MagB=np.append(MagB,result[4])
                     MagV=np.append(MagV,result[5])
                     MagR=np.append(MagR,result[6])
                     MagI=np.append(MagI,result[7])
                     MagJ=np.append(MagJ,result[8])
                     MagH=np.append(MagH,result[9])
                     MagK=np.append(MagK,result[10])
        MagMet=MagMet[1:]
        MagAge=MagAge[1:]/10**6
        MagU=MagU[1:]
        MagB=MagB[1:]
        MagV=MagV[1:]
        MagR=MagR[1:]
        MagI=MagI[1:]
        MagJ=MagJ[1:]
        MagH=MagH[1:]
        MagK=MagK[1:]
            
    print 'MagU',MagU
    print 'MagAge',MagAge
    # Calculate the Magnitude and the the luminosity for each Star
    if band=="u":
        print MagCalc(age[0],MagAge,MagU,5.56,mass[0],metal[0],MagMet)
        print MagCalc(age[1],MagAge,MagU,5.56,mass[1],metal[1],MagMet)

        myMagnitude=np.array([ MagCalc(age[x],MagAge,MagU,5.56,mass[x],metal[x],MagMet)  for x in np.arange(len(age))]) # MagSol taken from webpage indicated above
        print 'myMagnitude:',myMagnitude.shape,myMagnitude
        magst=myMagnitude[:,0]
        lumst=myMagnitude[:,1]
    if band=="b":
        print MagCalc(age[0],MagAge,MagB,5.45,mass[0],metal[0],MagMet)
        print MagCalc(age[1],MagAge,MagB,5.45,mass[1],metal[1],MagMet)
            
        myMagnitude=np.array([ MagCalc(age[x],MagAge,MagB,5.45,mass[x],metal[x],MagMet)  for x in np.arange(len(age))]) # MagSol taken from webpage indicated above
        print 'myMagnitude:',myMagnitude.shape,myMagnitude
        magst=myMagnitude[:,0]
        lumst=myMagnitude[:,1]
    if band=="k":
        print MagCalc(age[0],MagAge,MagK,3.28,mass[0],metal[0],MagMet)
        print MagCalc(age[1],MagAge,MagK,3.28,mass[1],metal[1],MagMet)
            
        myMagnitude=np.array([ MagCalc(age[x],MagAge,MagK,3.28,mass[x],metal[x],MagMet)  for x in np.arange(len(age))]) # MagSol taken from webpage indicated above
        print 'myMagnitude:',myMagnitude.shape,myMagnitude
        magst=myMagnitude[:,0]
        lumst=myMagnitude[:,1]
    if band=="r":
        print MagCalc(age[0],MagAge,MagR,4.46,mass[0],metal[0],MagMet)
        print MagCalc(age[1],MagAge,MagR,4.46,mass[1],metal[1],MagMet)

        myMagnitude=np.array([ MagCalc(age[x],MagAge,MagR,4.46,mass[x],metal[x],MagMet)  for x in np.arange(len(age))]) # MagSol taken from webpage indicated above
        print 'myMagnitude:',myMagnitude.shape,myMagnitude
        magst=myMagnitude[:,0]
        lumst=myMagnitude[:,1]
    if band=="i":
        print MagCalc(age[0],MagAge,MagI,4.46,mass[0],metal[0],MagMet)
        print MagCalc(age[1],MagAge,MagI,4.46,mass[1],metal[1],MagMet)
        
        myMagnitude=np.array([ MagCalc(age[x],MagAge,MagI,4.10,mass[x],metal[x],MagMet)  for x in np.arange(len(age))]) # MagSol taken from webpage indicated above
        print 'myMagnitude:',myMagnitude.shape,myMagnitude
        magst=myMagnitude[:,0]
        lumst=myMagnitude[:,1]
    if band=="v":
        print MagCalc(age[0],MagAge,MagV,4.80,mass[0],metal[0],MagMet)
        print MagCalc(age[1],MagAge,MagV,4.80,mass[1],metal[1],MagMet)
        
        myMagnitude=np.array([ MagCalc(age[x],MagAge,MagV,4.80,mass[x],metal[x],MagMet)  for x in np.arange(len(age))]) # MagSol taken fromwebpage indicated above
        print 'myMagnitude:',myMagnitude.shape,myMagnitude
        magst=myMagnitude[:,0]
        lumst=myMagnitude[:,1]
    #if metalflag=0 finishes. next else:
    

    print 'lumst',lumst.shape,lumst
                
                
    #Print the total magnitude of the galaxy for stars within 20 kpc for the chosen band
    print 'myind',np.where(r<20./simutokpc),np.array([np.where(r<20./simutokpc)][0][0]).shape
    print 'nparray',np.array([ lumst[i] for i in np.array([np.where(r<20./simutokpc)[0]][0]) ])
    TotLum=np.sum(np.array([lumst[i] for i in np.array([np.where(r<20./simutokpc)])[0][0]]),dtype=np.float64)
    print 'TotLum',TotLum
    if band=="u": 
        print 'and my '+band+'-band magnitude: ',5.56-2.5*log10(TotLum)
    if band=="b":
        print 'and my '+band+'-band magnitude: ',5.45-2.5*log10(TotLum)
    if band=="k":
        print 'and my '+band+'-band magnitude: ',3.28-2.5*log10(TotLum)
    if band=="r":
        print 'and my '+band+'-band magnitude: ',4.46-2.5*log10(TotLum)
    if band=="i":
        print 'and my '+band+'-band magnitude: ',4.10-2.5*log10(TotLum)
    if band=="v":
        print 'and my '+band+'-band magnitude: ',4.80-2.5*log10(TotLum)
    if StopFlag==1:
        print 'StopFlag activated. No outputfile generated!'
        exit()
    # instantiate a CunsOut object in "gadget2" format
    unso=CunsOut(nameout,"nemo")
    ok,tsnap=uns.getValueF("time") # return snasphot time
    print "Snapshot time : ","%.03f"%tsnap
    unso.setValueF("time",tsnap) # save snapshot time
    #data=np.reshape(data,(-1,3))
    data=np.append([posx],[posy],axis=0)
    data=np.append(data,[posz],axis=0)
    data=np.transpose(data)
    #print np.shape(data)
    #print data
    data=np.reshape(data,(1,-1))[0]
    print np.shape(data)
    print data

    vdata=np.append([vx],[vy],axis=0)
    vdata=np.append(vdata,[vz],axis=0)
    vdata=np.transpose(vdata)

    vdata=np.reshape(vdata,(1,-1))[0]

    #artificial hsml
    hsml=np.float32(0.001*np.ones(len(mass)))
    #if MetalFlag=1:
    metal=np.append([np.zeros(len(mass))],[metal],axis=0)
    print 'metal11',np.shape(metal),metal
    metal=np.append(metal,np.array([np.zeros(len(mass))]),axis=0)
    #print 'hsml',np.shape(hsml),hsml
    metal=np.transpose(metal)
    print 'metal2',np.shape(metal),metal
    metal=np.reshape(metal,(1,-1))[0]
    metal=np.float32(metal)
    metal=np.copy(metal)
    
    print 'Mag min max',magst.min(),magst.max(),'Mag min max',lumst.min(),lumst.max()
    
    
    unso.setArrayF("all","acc",metal) # save metalicity
    
    lumst=np.float32(lumst)
    unso.setArrayF("all","pos",data) # save pos
    unso.setArrayF("all","mass",mass/simutoMsun) # save mass
    unso.setArrayF("all","rho",lumst) # save magnitude in dens vector
    unso.setArrayF("all","vel",vdata) # save vel
    unso.setArrayF("all","pot",age) # save age
    unso.setArrayF("all","aux",hsml) # save artificial hsml otherwise glnemo2 doesn t visualize
    unso.save()
