#
#U=unigrid, Z=Zoom DM only, H=hydroLR
#box,halo,dm_m_0, bar_m_0 ,dm_m_f, bar_m_f
Bolson 1 3.99e+12 7.37e+11 2.44e+12 4.98e+11   
Bolson 2 1.58e+12 2.95e+11 9.10e+11 1.91e+11  
Cuba 1 1.61e+13 3.02e+12 1.14e+13 2.04e+12 
Cuba 2 1.62e+13 3.04e+12 1.11e+13 2.18e+12 
Mochima 3 1.43e+12 2.67e+11 9.67e+11 2.01e+11  
Mochima 5 1.90e+12 3.53e+11 1.07e+12 2.30e+11 
Turbach 2 1.69e+12 3.15e+11 7.48e+11 1.54e+11 
Turbach 5 1.61e+12 3.01e+11 9.18e+11 1.98e+11  
Yukon 1 231.1 1.162e+12 
Yukon 2 224.0 1.057e+12 

